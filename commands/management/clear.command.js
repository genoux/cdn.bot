const { Command } = require('discord.js-commando');

module.exports = class ClearCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'clear',
            group: 'management',
            memberName: 'clear',
            description: 'Clear message',
            aliases: ['cl','purge', 'delete'],
            examples: ['!clear 25'],
            clientPermissions: ['MANAGE_MESSAGES'],
            userPermissions: ['MANAGE_MESSAGES'],
            guildOnly: true,
            args: [
                {
                    key: 'number',
                    prompt: 'How many message do you wanna clear?',
                    type: 'string'
                }
            ]
        });
    }

   hasPermission(msg) {
        if (!this.client.isOwner(msg.author)) return 'Only the bot owner(s) may use this command.';
        return true;
    }

    run(msg, { number }, client) {  
        if(number > 99 || number == "all"){
            number = 99;
        }       
            msg.delete();
            msg.channel.bulkDelete(number);       
    }
};

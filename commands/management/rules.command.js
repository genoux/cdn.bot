const { Command } = require('discord.js-commando');
const { RichEmbed } = require('discord.js');
module.exports = class ClearCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'rules',
            group: 'management',
            memberName: 'rules',
            description: 'Read rules',
            examples: ['rules'],
           clientPermissions: ['MANAGE_MESSAGES'],
          // userPermissions: ['MANAGE_MESSAGES'],
            guildOnly: false,
        });
    }

    run(msg, client) {  
     
        msg.channel.send(msg.author + ' I sent you a msg about rules')   
        msg.author.send({
            embed: {
                color: 15808315,
                title: "FR - RÈGLES GÉNÉRALE",
                description: "What's up guys! Bienvenue sur mon serveur! Ici vous trouverez des gens qui jouent à toutes sorte de jeux, que ce soit compétitif ou juste casual. Cependant Quelques règles sont à respecter",
                fields: [{
                    name: "1) Rien de cette liste sera toléré:",
                    value: "• Partage de vos liens de réseaux sociaux. \n • Aucune insulte pour but de blesser ou d'intimider les autres joueurs \n • Aucun contenu à caractère sexuel \n • Aucun propos raciste, sexiste et/ou homophobe \n • Les spams "
                },
                {
                    name: "2) Utilisation des channels",
                    value: "Utilisez les channels correspondants au jeux auquels vous jouer pour évitez les conufusions"
                },
                {
                    name: "3) Ayez du plaisir!",
                    value: "N'oublions pas que nous sommes ici pour avoir du fun!"
                },
                {
                    name: "4) Autres",
                    value: "Pour le reste soyez le bienvenue de parler avec les autres et moi même pour team up. Si vous avez d'autre questions n'hesitez pas à me @mention, cheers!"
                }
                ],
                footer: {
                    icon_url: bot.user.avatarURL,
                    text: "Tu peux aussi te référer au channel @welcome"
                }
            }
        })
        
        msg.author.send({
            embed: {
                color: 15808315,
                title: "EN - GENERAL RULES",
                description: "What's up guys! Welcome to  my channel! Here you'll find alot of people who play a  bunch of different games from casual to competitive so feel free to join them. Although there is a few simple rules to follow:",
                fields: [{
                    name: "1) Nothing of this list will be tolerated:",
                    value: "• Sharing your social media links \n • It is forbidden to insult players for the purpose of hurting them or intimidate them \n • No sexist, rasicist and/or homophobic statement is tolerated \n • Spams "
                },
                {
                    name: "2) Channel Utility",
                    value: "Please use the appropriate channel corresponding your game to prevent any confusion."
                },
                {
                    name: "3) You guys have fun!",
                    value: "Don't forget that we are here to have fun!"
                },
                {
                    name: "4) Other",
                    value: "Please feel free to speak to the others and my self to team up! If you have any other question dont hesitate to @mention me or dm me. Cheers!"
                }
                ],
                footer: {
                    icon_url: bot.user.avatarURL,
                    text: "You can also refer to the @welcome channel."
                }
            }
        }); 
    }
};

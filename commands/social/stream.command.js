const {
    Command
} = require('discord.js-commando');
let Twitch = require('../../lib/services/twitch.services');
const {
    RichEmbed
} = require("discord.js");

module.exports = class StreamCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'stream',
            group: 'social',
            memberName: 'stream',
            description: 'Get info on stream',
            examples: ['!stream'],
            aliases: ['st', 'live','cdn'],
            // clientPermissions: ['MANAGE_MESSAGES'],
            //userPermissions: ['MANAGE_MESSAGES'],
            guildOnly: true,
        });
    }

    run(msg, client) {
        Twitch.GetStream().then(stream => {
       Twitch.StreamFetch(stream,msg);
    });

}
};
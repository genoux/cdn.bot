const {
    Command
} = require('discord.js-commando');
var urban = require('urban');
const {
    RichEmbed
} = require("discord.js");
module.exports = class UrbanCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'urban',
            group: 'random',
            memberName: 'urban',
            aliases: ["urb","ub"],
            description: 'Get urban dictonary of a word',
            examples: ['!urban word'],
            args: [{
                key: 'word',
                prompt: 'What word are you looking for?',
                type: 'string'
            }]
        });
    }


    run(msg, {word}) {

        if (word == "random") {
            urban.random().first(function (data) {
                console.log(data);
                const embed = new RichEmbed({
                    color: 1265638,
                    title: "**" + data.word + "**",
                    description: data.definition,
                    fields: [{
                        name: "**Example**",
                        value: data.example
                    }]
                });
                msg.channel.send({
                    embed
                })
            });
        } else {

            urban(word).first(function (data) {
                if (!data) {
                    msg.channel.send("Nothing found... :thermometer_face:")
                    return;
                }
        
                console.log(data);
                const embed = new RichEmbed({
                    color: 1265638,
                    title: "**" + data.word + "**",
                    description: data.definition,
                    fields: [{
                        name: "**Example**",
                        value: data.example
                    }]
                });
                msg.channel.send({
                    embed
                })
            });
        }



        //return msg.say('Nice, you rolled a **' + value +'** !');
    }
};
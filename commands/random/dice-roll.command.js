const { Command } = require('discord.js-commando');

module.exports = class ReplyCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'roll',
            group: 'random',
            memberName: 'roll',
            description: 'Roll a dice',
            examples: ['roll'],
        });
    }


    run(msg, { text }) {
        const value = Math.random()*6 + 1 | 0; 
        return msg.say('Nice, you rolled a **' + value +'** !');
    }
};
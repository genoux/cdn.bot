var http = require('http'),
  axios = require("axios"),
  fs = require('fs'),
  querystring = require('querystring'),
  subtext = require('subtext');
const {
  RichEmbed
} = require("discord.js");

function GetUserID() {
  return axios({
      method: "get",
      url: " https://api.twitch.tv/helix/users?login=genouxx",
      headers: {
        "Client-ID": config.clientID
      }
    }).then(function (response) {
      this.elm = response.data.data[0];
      console.log('this.elm ', this.elm);
      return this.elm
    })
    .catch(function (error) {
      console.log(error);
    });
}


//getStream will give you the stream status: is it live, whats the game, nb viewers..etc
//getUser will give you: user id, profile img, total view count...etc
let userID = 142958346;
//genouxx = 40543068
//cdn = 142958346
//kit = 32787655
//nihja = 19571641
//xqc = 71092938


function GetStream() {

  return axios({
      method: "get",
      url: "https://api.twitch.tv/helix/streams?user_id=" + userID,
      headers: {
        "Client-ID": config.clientID
      }
    }).then(function (response) {
      this.elm = response.data.data[0];
      return this.elm
    })
    .catch(function (error) {
      console.log(error);
    });
}

function GetUser() {
  return axios({
      method: "get",
      url: " https://api.twitch.tv/helix/users?id=" + userID,
      headers: {
        "Client-ID": config.clientID
      }
    }).then(function (response) {
      this.elm = response.data.data[0];
      return this.elm
    })
    .catch(function (error) {
      console.log(error);
    });
}

function GetGameName(id) {
  return axios({
      method: "get",
      url: "https://api.twitch.tv/helix/games?id=" + id,
      headers: {
        "Client-ID": config.clientID
      }
    }).then(function (response) {
      this.elm = response.data.data[0];
      return this.elm
    })
    .catch(function (error) {
      console.log(error);
    })
}



axios({
    method: "post",
    url: "https://api.twitch.tv/helix/webhooks/hub",
    headers: {
      "Client-ID": config.clientID
    },
    data: {
      "hub.callback": "http://32dae819.ngrok.io",
      "hub.mode": "subscribe",
      "hub.topic": "https://api.twitch.tv/helix/streams?user_id=" + userID,
      "hub.lease_seconds": 864000
    },
    validateStatus: function (status) {
      return status >= 200 && status < 300; // default
    },
  })
  .then(function (response) {
    console.log('response', response.status);
  })
  .catch(function (error) {
    console.log(error);
  });

http.createServer((request, response) => {

    let body = '';
    var oQueryParams;
    let hubChallenge = '';
    let jsonContent;
    if (request.url.indexOf("?") >= 0) {
      oQueryParams = querystring.parse(request.url.replace(/^.*\?/, ""));
      hubChallenge = oQueryParams['hub.challenge'];
    }
    const {
      headers,
      method,
      url
    } = request;

    request.on("error", err => {
        console.error(err);
      })
      .on("data", chunk => {
        body += chunk;
        console.log("Partial body received: " + chunk);
        jsonContent = JSON.parse(body).data[0];
        StreamFetch(jsonContent, null);


        return;
      })
      .on("end", () => {
        response.on("error", err => {
          console.error(err);
        });

        response.writeHead(200, {
          "Content-Type": 'text/html'
        });
        const responseBody = {
          headers,
          method,
          url,
          body
        };

        response.write(hubChallenge)
        console.log("RES statusCode: ", response.statusCode);
        response.end()


    });
  })
  .listen(config.PORT);
console.log('Server running at http://127.0.0.1:' + config.PORT);



//sending message to the receipt, with stream data
//msg may be null
function StreamFetch(stream, msg) {
  //if object of stream is null, stream is offline
  if (stream == undefined && msg != null) {
    console.log("stream is offline")
    const embed = {
      "title": "Stream is offline :dizzy_face: ",
      "color": 1907997
    };
    bot.channels.get(msg.message.channel.id).send({
      embed
    });
    
  } else {
    console.log("stream is online!")

    GetUser().then(user => {
      GetGameName(stream.game_id).then(gameName => {

        let thumbnail_url = stream.thumbnail_url;
        thumbnail_url = thumbnail_url.replace('{width}', '1920')
        thumbnail_url = thumbnail_url.replace('{height}', '1080')
        const embed = new RichEmbed({
          color: 5846167,
          title: ":red_circle: " + "**" + user.display_name + "**" + " is live!!",
          url: "https://www.twitch.tv/" + user.login,
          fields: [{
              name: stream.title,
              value: "**Viewers:** " + stream.viewer_count
            },
            {
              name: ':video_game:',
              value: '**Current Game: ** ' + gameName.name
            }
          ]
        }).setImage(thumbnail_url).setThumbnail(user.profile_image_url)
        bot.channels.get('473620790382821377').send('@everyone', {
          embed
        })

      });
    });
  }

}

module.exports = {
  GetStream: GetStream,
  GetGameName: GetGameName,
  GetUser: GetUser,
  StreamFetch: StreamFetch
}
const {
  CommandoClient
} = require("discord.js-commando");

let badWord = require('./bad-word.services')
let twitchServices = require('./twitch.services');
bodyParser = require("body-parser"),
  path = require("path");

global.bot = new CommandoClient({
  commandPrefix: config.prefix,
  owner: "75069008939847680",
  disableEveryone: true
});

bot.registry
  .registerDefaultTypes()
  .registerGroups([
    ["random", "Random"],
    ["management", "management"],
    ["social", "social"]
  ])
  .registerDefaultGroups()
  .registerDefaultCommands({
    help: true
  })
  .registerCommandsIn(path.join(__dirname, "../../commands"));

bot.on("ready", () => {
  // Set the client user's presence
  console.log("Logged as " + bot.user.username);
  twitchServices.GetStream().then(stream => {
    twitchServices.GetGameName(stream.game_id).then(gameName => {
      bot.user.setActivity(gameName.name);
    });
  });
});

bot.on('message', msg => {

   let filterWord=  badWord.msgFilter(msg)
   console.log('filterWord', filterWord);
   if(filterWord){
     msg.delete()
   .then(msg => {
     console.log(`Deleted message from ${msg.author.username}`)
     msg.channel.send(msg.author + ' Yiooo check your langage dummy!')
   })
   .catch(console.error);
   }


});
bot.login(config.discordToken);
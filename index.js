const fs = require('fs');
const path = require('path');
const modulesPath = './lib/services';
const modulesList = fs.readdirSync(modulesPath);
var express = require("express"),
helmet = require('helmet');

app = express()
app.use(helmet())

global.config = require("./conf/config")
  
//Main Module so the bot can be alive
require("./lib/services/discord.services");

//get other module in lib/services
modulesList.forEach(modulePath => require(path.resolve(modulesPath, modulePath)));


